package com.company;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class InfoUpdate {
    public static ConcurrentHashMap<String, String> update(ConcurrentHashMap<String, String> info) {

        info.put("temperature", Integer.toString(new Random().nextInt(20)));
        info.put("pressure", Integer.toString(new Random().nextInt(20)));
        info.put("wetness", Integer.toString(new Random().nextInt(20)));

        return info;
    }
}
