package com.company;

import java.util.concurrent.ConcurrentHashMap;

public final class LocalizationRU {

    public static ConcurrentHashMap<String, String> getLocalizationRU() {

        ConcurrentHashMap<String, String> localeMap = new ConcurrentHashMap<>();

        localeMap.put("name","лондон");
        localeMap.put("temperature","температура");
        localeMap.put("pressure","давление");
        localeMap.put("wetness","влажность");

        return localeMap;

    }
}
