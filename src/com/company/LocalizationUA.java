package com.company;

import java.util.concurrent.ConcurrentHashMap;

public final class LocalizationUA {
    public static ConcurrentHashMap<String, String> getLocalizationUA() {

        ConcurrentHashMap<String, String> localeMap = new ConcurrentHashMap<>();

        localeMap.put("name","лондон");
        localeMap.put("temperature","температура");
        localeMap.put("pressure","тиск");
        localeMap.put("wetness","вологiсть");

        return localeMap;

    }
}
