package com.company;

import java.util.concurrent.ConcurrentHashMap;

public final class LondonInfo {
    public static ConcurrentHashMap getInfo() {

        ConcurrentHashMap<String, String> londonMap = new ConcurrentHashMap<>();

        londonMap.put("name","london");
        londonMap.put("temperature","18");
        londonMap.put("pressure","1");
        londonMap.put("wetness","13");


        return londonMap;
    }
}
