package com.company;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Main {

    private static ConcurrentHashMap<String, String> londonInfo;
    private static AtomicLong changeTime = new AtomicLong();

    public static void main(String[] args) {


        new Thread(() -> {
            while (true) {
                londonInfo = InfoUpdate.update(LondonInfo.getInfo());
                changeTime.set(System.currentTimeMillis());
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        getThread(LocalizationRU.getLocalizationRU()).start();

        getThread(LocalizationUA.getLocalizationUA()).start();

        getThread(LocalizationMars.getLocalizationMars()).start();


    }

    private static Thread getThread(ConcurrentHashMap<String, String> local) {
        return new Thread(() -> {
            long lastChange = 0;
            while (true) {
                if (lastChange < changeTime.get()) {
                    lastChange = changeTime.get();
                    for (String key : londonInfo.keySet()) {
                        System.out.println(local.get(key));
                        System.out.println(londonInfo.get(key));

                    }
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }
}
